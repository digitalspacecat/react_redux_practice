/**
 * Created by chloe on 27/11/2016.
 */
import React from 'react';
import VideoListItem from './video_list_item';

const VideoList = (props) => {
  const videoItems = props.videos.map((video) => {
    return (
      <VideoListItem
        onVideoSelect={props.onVideoSelect}
        key={video.etag}
        video={video} />
    );
  });

  return (
    <ul className="col-md-4 list-group">
      {/*{props.videos.length} <= why this brings the number of items? */}
      {videoItems}
    </ul>
  );
};

export default VideoList;