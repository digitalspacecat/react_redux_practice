# Learn Redux

A simple React + Redux implementation. I am following the tutorial made by wesbos.

This is starter stage what he provides to learners and I will customise UI later. 


## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:7770> in your browser.

## Production Build

Run `npm build` to create a distro folder and a bundle.js file.
